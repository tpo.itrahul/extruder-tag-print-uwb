﻿Public Class FrmHold
    Public Dsbtnvalue As New DataSet()
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Dim objcon As New Connections
    Private Sub FrmHold_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GlobalVariables.pkdextid = GlobalVariables.idvalue
        cboShift.Items.Add("Air Under Cusion")
        cboShift.Items.Add("Lumpy (Cap/Base/Cusion)")
        cboShift.Items.Add("Length Variation")
        cboShift.Items.Add("Less Overall Width")
        cboShift.Items.Add("Wrong Identification")
        cboShift.Items.Add("Over Weight")
        cboShift.Items.Add("Under Weight")
        cboShift.Items.Add("FM Condamination")
        cboShift.Items.Add("Tech Trial")
        cboShift.Items.Add("Online Swap")
        cboShift.SelectedIndex = 0
    End Sub
    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        GlobalVariables.LoginName = txtClockno.Text
        GlobalVariables.Position = cboShift.Text
        If txtClockno.Text <> "" And cboShift.Text <> "" Then
            objcon.insertHOLD()
            txtClockno.Text = ""
            ''txtReason.Text = ""
            Me.Close()
            FrmTakentoTB.Show()
        Else
            MessageBox.Show("'Enter All Details....!!!!'")
        End If
    End Sub
End Class