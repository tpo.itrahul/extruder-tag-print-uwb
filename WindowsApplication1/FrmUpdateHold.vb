﻿Imports QRCoder
Imports System.IO
Imports System.Drawing.Printing

Public Class FrmUpdateHold
    Dim objcon As New Connections
    Dim dt As New DataTable
    Public SelectedShift As String
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Public ddt As DataTable
    Private Sub FrmUpdateHold_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtId.Enabled = True
        txtItemCode.Enabled = False
        txtColourCode.Enabled = False
        txtLTruckNo.Enabled = False
        txtLocation.Enabled = True
        txtQty.Enabled = False
        txtNo.Enabled = False
        btnPrint.Enabled = False
        CboLocation.Enabled = False
        CboLeafTruck.Enabled = False
    End Sub
    Private Sub btnEnter_Click(sender As Object, e As EventArgs) Handles btnEnter.Click
        If txtId.Text <> "" Then
            btnPrint.Enabled = True
            txtQty.Enabled = True
            CboLocation.Enabled = True
            CboLeafTruck.Enabled = True
            GlobalVariables.id = txtId.Text
            dt = objcon.HoldDetRetrieval().Tables(0)
            If dt.Rows.Count > 0 Then
                txtItemCode.Text = Convert.ToString(dt.Rows(0).ItemArray(4))
                txtColourCode.Text = Convert.ToString(dt.Rows(0).ItemArray(19))
                CboLeafTruck.Text = Convert.ToString(dt.Rows(0).ItemArray(6))
                CboLocation.Text = Convert.ToString(dt.Rows(0).ItemArray(8))
                txtQty.Text = Convert.ToString(dt.Rows(0).ItemArray(7))
                txtNo.Text = Convert.ToString(dt.Rows(0).ItemArray(22))


                'If txtColourCode.Text.StartsWith("SW") Then
                '    GlobalVariables.Defect_Type = "SW"
                'Else
                '    GlobalVariables.Defect_Type = "other"
                'End If

                GlobalVariables.SelectionTreadCode = txtItemCode.Text
                GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))

                ddt = objcon.GetCompound().Tables(0)
                txtLocation.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))
                GlobalVariables.Tyre = txtLocation.Text

                LocationLoad()
                CodeLoad()

            Else
                MessageBox.Show("No Data Found")
                txtId.Text = ""
                btnPrint.Enabled = False
                txtItemCode.Text = ""
                txtColourCode.Text = ""
                CboLeafTruck.Text = ""
                CboLocation.Text = ""
                txtQty.Text = ""
                txtNo.Text = ""
                txtLTruckNo.Text = ""
            End If
        Else
            MessageBox.Show("Enter the Id")
        End If
    End Sub

    Public Sub LocationLoad()
        Dim dtab1 As New DataSet()


        dtab1 = objcon.GetLocation1()


        Dim dr As DataRow
        For Each dr In dtab1.Tables(0).Rows
            CboLocation.Items.Add(dr("Location"))
        Next


    End Sub
    Public Sub CodeLoad()
        Dim dtab1 As New DataSet()
        dtab1 = objcon.GetCode()

        Dim dr As DataRow
        For Each dr In dtab1.Tables(0).Rows
            CboLeafTruck.Items.Add(dr("LeafTruck_No"))
        Next
        'CboLeafTruck.DisplayMember = "LeafTruck_No"
        'CboLeafTruck.DataSource = dtab.Tables(0)

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        btnPrint.Enabled = False

        Dim pkextid As Single = objcon.get_DextPKID()
        GlobalVariables.DescId = pkextid

        dt = objcon.HoldDetRetrieval().Tables(0)
        If dt.Rows.Count > 0 Then
            GlobalVariables.Id1 = Convert.ToString(dt.Rows(0).ItemArray(0))
            GlobalVariables.extdate = Convert.ToDateTime(dt.Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
            GlobalVariables.exttime = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("HH:mm:ss tt")

            GlobalVariables.Press = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("hh:mm tt")

            GlobalVariables.extshift = Convert.ToString(dt.Rows(0).ItemArray(3))
            GlobalVariables.SelectionTreadCode = Convert.ToString(dt.Rows(0).ItemArray(4))



            GlobalVariables.description = Convert.ToString(dt.Rows(0).ItemArray(5))
            'GlobalVariables.Code = Convert.ToString(dt.Rows(0).ItemArray(6))
            GlobalVariables.Code = CboLeafTruck.Text

            GlobalVariables.SerialNo = CboLeafTruck.Text
            GlobalVariables.oldLeaf = Convert.ToString(dt.Rows(0).ItemArray(6))

            GlobalVariables.Qty = Convert.ToString(dt.Rows(0).ItemArray(7))

            GlobalVariables.oldLocation = Convert.ToString(dt.Rows(0).ItemArray(8))

            GlobalVariables.Location = CboLocation.Text
            GlobalVariables.curdate = Convert.ToDateTime(dt.Rows(0).ItemArray(9)).ToString("yyyy/MM/dd HH:mm:ss")



            GlobalVariables.No = Convert.ToString(dt.Rows(0).ItemArray(22))
            GlobalVariables.ItemCode = Convert.ToString(dt.Rows(0).ItemArray(19))
            GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))
            GlobalVariables.Username = Convert.ToString(dt.Rows(0).ItemArray(21))

            GlobalVariables.HId = Convert.ToString(dt.Rows(0).ItemArray(29))

        End If


        'If GlobalVariables.ItemCode.StartsWith("SW") Then
        '    GlobalVariables.Defect_Type = "SW"
        'Else
        '    GlobalVariables.Defect_Type = "other"
        'End If

        objcon.DeleteTreadH()
        'newupdate20.04.2018
        'objcon.UpdateNewID()
        objcon.TrueUpdateOldLocation()
        objcon.TrueUpdateLocation()

        If txtColourCode.Text.StartsWith("SW") Then
            GlobalVariables.Defect_Type = "SW"
            objcon.TrueUpdateLeaftruckSW()
            objcon.TrueUpdateoldLeaftruckSW()
        Else
            GlobalVariables.Defect_Type = "other"
            objcon.TrueUpdateLeaftruck()
            objcon.TrueUpdateoldLeaftruck()
        End If

      
      
        GlobalVariables.Qty = txtQty.Text

        GlobalVariables.T1 = GlobalVariables.Code
        GlobalVariables.T2 = CboLocation.Text
        GlobalVariables.T3 = txtQty.Text
        GlobalVariables.T4 = GlobalVariables.No

        Dim C1 As Single = objcon.Check_EntryCount()
        If C1 <= 0 Then
            objcon.insertTreadEntery2()
            objcon.FalseUpdateLocation()
            If txtColourCode.Text.StartsWith("SW") Then
                GlobalVariables.Defect_Type = "SW"
                objcon.FalseUpdateLeaftruckSW()
            Else
                GlobalVariables.Defect_Type = "other"
              objcon.FalseUpdateLeaftruck()
            End If

            objcon.UpdateHoldStatus()


            'objcon.FalseUpdateLeaftruck()
            'objcon.FalseUpdateLocation()
            'objcon.FalseUpdateLeaftruck()

            'GlobalVariables.id = txtId.Text


            If txtColourCode.Text.StartsWith("SW") Then
                Dim pd1 As New PrintDocument
                AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                pd1.PrintController = New StandardPrintController()

                pd1.DefaultPageSettings.Landscape = False
                pd1.PrinterSettings.Copies = "1"

                pd1.Print()
            Else
                Dim pd As New PrintDocument
                AddHandler pd.PrintPage, AddressOf Me.PrintDocument1_PrintPage
                pd.PrintController = New StandardPrintController()

                pd.DefaultPageSettings.Landscape = False
                pd.PrinterSettings.Copies = "1"

                pd.Print()

            End If

            txtId.Text = ""
            txtItemCode.Text = ""
            txtColourCode.Text = ""
            txtLTruckNo.Text = ""
            CboLocation.Text = ""
            txtQty.Text = ""
            txtNo.Text = ""
            CboLocation.Text = ""
            CboLocation.Items.Clear()
            LocationLoad()
            CboLeafTruck.Text = ""
            '   CboLeafTruck.Items.Clear()
            CodeLoad()
         
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        btnPrint.Enabled = False
        txtId.Text = ""
        txtItemCode.Text = ""
        txtColourCode.Text = ""
        txtLTruckNo.Text = ""
        CboLocation.Text = ""
        txtQty.Text = ""
        txtNo.Text = ""
        CboLeafTruck.Text = ""
        CboLocation.Items.Clear()

    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage



        '  Lblcomp.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))


        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 240, 30)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("TREAD", printFontHeadHighest, Brushes.Black, 50, 70)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 180, 20)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(GlobalVariables.ItemCode, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TREAD", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(GlobalVariables.SelectionTreadCode, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(GlobalVariables.Qty, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)
        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(GlobalVariables.Username, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.extdate & "--" & GlobalVariables.extshift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.Press, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("LT NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("TR NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(GlobalVariables.No, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("COMP :", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(GlobalVariables.Tyre, printFont, Brushes.Black, 100, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


    End Sub


    Private Sub PrintDocument2_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage



        '  Lblcomp.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))


        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.DescId, printFontSmall, Brushes.Black, 240, 30)

        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.DescId, QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("SIDEWALL", printFontHeadHighest, Brushes.Black, 50, 70)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 180, 20)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(GlobalVariables.ItemCode, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SIDEWALL", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(GlobalVariables.SelectionTreadCode, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(GlobalVariables.Qty, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)
        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("OPERATOR", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(GlobalVariables.Username, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("DATE/SHIFT", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.extdate & "--" & GlobalVariables.extshift, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TIME", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.Press, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("SW NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("SL NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(GlobalVariables.No, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString("", printFont, Brushes.Black, 100, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


    End Sub

End Class