﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCurrent = New System.Windows.Forms.Button()
        Me.btnLocation = New System.Windows.Forms.Button()
        Me.BtnHold = New System.Windows.Forms.Button()
        Me.BtnManual = New System.Windows.Forms.Button()
        Me.btnScrap = New System.Windows.Forms.Button()
        Me.btnAged = New System.Windows.Forms.Button()
        Me.btnProd = New System.Windows.Forms.Button()
        Me.btnscraprep = New System.Windows.Forms.Button()
        Me.btnTakentoTB = New System.Windows.Forms.Button()
        Me.btnTBRej = New System.Windows.Forms.Button()
        Me.btnDelHistory = New System.Windows.Forms.Button()
        Me.BtnHoldRep = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCurrent
        '
        Me.btnCurrent.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnCurrent.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCurrent.ForeColor = System.Drawing.Color.White
        Me.btnCurrent.Location = New System.Drawing.Point(58, 32)
        Me.btnCurrent.Name = "btnCurrent"
        Me.btnCurrent.Size = New System.Drawing.Size(211, 41)
        Me.btnCurrent.TabIndex = 87
        Me.btnCurrent.Text = "Current Stock"
        Me.btnCurrent.UseVisualStyleBackColor = False
        '
        'btnLocation
        '
        Me.btnLocation.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnLocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLocation.ForeColor = System.Drawing.Color.White
        Me.btnLocation.Location = New System.Drawing.Point(316, 32)
        Me.btnLocation.Name = "btnLocation"
        Me.btnLocation.Size = New System.Drawing.Size(211, 41)
        Me.btnLocation.TabIndex = 86
        Me.btnLocation.Text = "Location Update"
        Me.btnLocation.UseVisualStyleBackColor = False
        '
        'BtnHold
        '
        Me.BtnHold.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.BtnHold.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnHold.ForeColor = System.Drawing.Color.White
        Me.BtnHold.Location = New System.Drawing.Point(569, 32)
        Me.BtnHold.Name = "BtnHold"
        Me.BtnHold.Size = New System.Drawing.Size(211, 41)
        Me.BtnHold.TabIndex = 88
        Me.BtnHold.Text = "Hold Update"
        Me.BtnHold.UseVisualStyleBackColor = False
        '
        'BtnManual
        '
        Me.BtnManual.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.BtnManual.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnManual.ForeColor = System.Drawing.Color.White
        Me.BtnManual.Location = New System.Drawing.Point(316, 93)
        Me.BtnManual.Name = "BtnManual"
        Me.BtnManual.Size = New System.Drawing.Size(211, 41)
        Me.BtnManual.TabIndex = 89
        Me.BtnManual.Text = "Manual Entry"
        Me.BtnManual.UseVisualStyleBackColor = False
        '
        'btnScrap
        '
        Me.btnScrap.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnScrap.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScrap.ForeColor = System.Drawing.Color.White
        Me.btnScrap.Location = New System.Drawing.Point(58, 93)
        Me.btnScrap.Name = "btnScrap"
        Me.btnScrap.Size = New System.Drawing.Size(211, 41)
        Me.btnScrap.TabIndex = 90
        Me.btnScrap.Text = "Scrap  Entry"
        Me.btnScrap.UseVisualStyleBackColor = False
        '
        'btnAged
        '
        Me.btnAged.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnAged.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAged.ForeColor = System.Drawing.Color.White
        Me.btnAged.Location = New System.Drawing.Point(569, 93)
        Me.btnAged.Name = "btnAged"
        Me.btnAged.Size = New System.Drawing.Size(211, 41)
        Me.btnAged.TabIndex = 91
        Me.btnAged.Text = "Over Aged"
        Me.btnAged.UseVisualStyleBackColor = False
        '
        'btnProd
        '
        Me.btnProd.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnProd.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProd.ForeColor = System.Drawing.Color.White
        Me.btnProd.Location = New System.Drawing.Point(58, 212)
        Me.btnProd.Name = "btnProd"
        Me.btnProd.Size = New System.Drawing.Size(211, 41)
        Me.btnProd.TabIndex = 92
        Me.btnProd.Text = "Production Report"
        Me.btnProd.UseVisualStyleBackColor = False
        '
        'btnscraprep
        '
        Me.btnscraprep.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnscraprep.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnscraprep.ForeColor = System.Drawing.Color.White
        Me.btnscraprep.Location = New System.Drawing.Point(316, 212)
        Me.btnscraprep.Name = "btnscraprep"
        Me.btnscraprep.Size = New System.Drawing.Size(211, 41)
        Me.btnscraprep.TabIndex = 93
        Me.btnscraprep.Text = "Scrap Report"
        Me.btnscraprep.UseVisualStyleBackColor = False
        '
        'btnTakentoTB
        '
        Me.btnTakentoTB.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnTakentoTB.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTakentoTB.ForeColor = System.Drawing.Color.White
        Me.btnTakentoTB.Location = New System.Drawing.Point(569, 212)
        Me.btnTakentoTB.Name = "btnTakentoTB"
        Me.btnTakentoTB.Size = New System.Drawing.Size(211, 41)
        Me.btnTakentoTB.TabIndex = 94
        Me.btnTakentoTB.Text = "Taken to TB Report"
        Me.btnTakentoTB.UseVisualStyleBackColor = False
        '
        'btnTBRej
        '
        Me.btnTBRej.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnTBRej.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTBRej.ForeColor = System.Drawing.Color.White
        Me.btnTBRej.Location = New System.Drawing.Point(569, 271)
        Me.btnTBRej.Name = "btnTBRej"
        Me.btnTBRej.Size = New System.Drawing.Size(211, 41)
        Me.btnTBRej.TabIndex = 95
        Me.btnTBRej.Text = "TB Rejection"
        Me.btnTBRej.UseVisualStyleBackColor = False
        Me.btnTBRej.Visible = False
        '
        'btnDelHistory
        '
        Me.btnDelHistory.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnDelHistory.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelHistory.ForeColor = System.Drawing.Color.White
        Me.btnDelHistory.Location = New System.Drawing.Point(316, 154)
        Me.btnDelHistory.Name = "btnDelHistory"
        Me.btnDelHistory.Size = New System.Drawing.Size(211, 41)
        Me.btnDelHistory.TabIndex = 96
        Me.btnDelHistory.Text = "Delete History"
        Me.btnDelHistory.UseVisualStyleBackColor = False
        '
        'BtnHoldRep
        '
        Me.BtnHoldRep.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.BtnHoldRep.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnHoldRep.ForeColor = System.Drawing.Color.White
        Me.BtnHoldRep.Location = New System.Drawing.Point(569, 154)
        Me.BtnHoldRep.Name = "BtnHoldRep"
        Me.BtnHoldRep.Size = New System.Drawing.Size(211, 41)
        Me.BtnHoldRep.TabIndex = 97
        Me.BtnHoldRep.Text = "Hold Report"
        Me.BtnHoldRep.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold)
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(60, 154)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(209, 46)
        Me.Button1.TabIndex = 98
        Me.Button1.Text = "Leaf Truck Report"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(60, 271)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(211, 41)
        Me.Button2.TabIndex = 99
        Me.Button2.Text = "Inventory"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmSelection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(837, 324)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BtnHoldRep)
        Me.Controls.Add(Me.btnDelHistory)
        Me.Controls.Add(Me.btnTBRej)
        Me.Controls.Add(Me.btnTakentoTB)
        Me.Controls.Add(Me.btnscraprep)
        Me.Controls.Add(Me.btnProd)
        Me.Controls.Add(Me.btnAged)
        Me.Controls.Add(Me.btnScrap)
        Me.Controls.Add(Me.BtnManual)
        Me.Controls.Add(Me.BtnHold)
        Me.Controls.Add(Me.btnCurrent)
        Me.Controls.Add(Me.btnLocation)
        Me.Name = "FrmSelection"
        Me.Text = "FrmSelection"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCurrent As System.Windows.Forms.Button
    Friend WithEvents btnLocation As System.Windows.Forms.Button
    Friend WithEvents BtnHold As System.Windows.Forms.Button
    Friend WithEvents BtnManual As System.Windows.Forms.Button
    Friend WithEvents btnScrap As System.Windows.Forms.Button
    Friend WithEvents btnAged As System.Windows.Forms.Button
    Friend WithEvents btnProd As System.Windows.Forms.Button
    Friend WithEvents btnscraprep As System.Windows.Forms.Button
    Friend WithEvents btnTakentoTB As System.Windows.Forms.Button
    Friend WithEvents btnTBRej As System.Windows.Forms.Button
    Friend WithEvents btnDelHistory As System.Windows.Forms.Button
    Friend WithEvents BtnHoldRep As System.Windows.Forms.Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
