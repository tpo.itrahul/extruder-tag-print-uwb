﻿Imports QRCoder
Imports System.IO
Imports System.Drawing.Printing
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Data.SqlClient
Imports System.Net
Imports System.Text


Public Class FrmTech_Qa
    Dim objcon As New Connections
    Dim dt As New DataTable
    Public SelectedShift As String
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Public ddt As DataTable
    Public DsSelection As DataSet

    Private Sub FrmTech_Qa_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CboReason.Items.Add("Air Under Cusion")
        CboReason.Items.Add("Defective Cushion")
        CboReason.Items.Add("Defective Skiving")
        CboReason.Items.Add("Depreciation")
        CboReason.Items.Add("Excess Width")
        CboReason.Items.Add("FM Condamination")
        CboReason.Items.Add("Length Variation")
        CboReason.Items.Add("Lumpy (Cap/Base/Cusion)")
        CboReason.Items.Add("Less Overall Width")
        CboReason.Items.Add("Mid Cap Offcenter")
        CboReason.Items.Add("No Central Line")
        CboReason.Items.Add("Online Scrap")
        CboReason.Items.Add("Over Weight")
        CboReason.Items.Add("Profile Variation")
        CboReason.Items.Add("TB Rejection")
        CboReason.Items.Add("Tech Trial")
        CboReason.Items.Add("Torn Edge")
        CboReason.Items.Add("Under Weight")
        CboReason.Items.Add("Wrong Identification")

        CboReason.SelectedIndex = 0

        LblOperaor.Text = GlobalVariables.Username
        Lblname.Text = GlobalVariables.Name

        txtId.Enabled = True
        txtItemCode.Enabled = False
        txtColourCode.Enabled = False
        txtLTruckNo.Enabled = False
        txtLocation.Enabled = True
        txtQty.Enabled = False
        txtNo.Enabled = False
        btnPrint.Enabled = False
        txtLoc.Enabled = False
        txtLTruckNo.Enabled = False
        BindCurrentDateandShift()
    End Sub
    Private Sub BindCurrentDateandShift()
        Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime
        Dim now As DateTime = DateTime.Now
        Dim curTime As DateTime = DateTime.Now
        ShiftA = Convert.ToDateTime("06:01:00 AM")
        ShiftB = Convert.ToDateTime("02:01:00 PM")
        ShiftC = Convert.ToDateTime("10:01:00 PM")
        EndDayTime = Convert.ToDateTime("11:59:55 PM")
        NextDayStartTime = Convert.ToDateTime("12:00:00 AM")
        If curTime >= ShiftA AndAlso curTime < ShiftB Then

            lblShift0.Text = "A"
        ElseIf curTime >= ShiftB AndAlso curTime < ShiftC Then
            lblShift0.Text = "B"
        ElseIf curTime >= ShiftC AndAlso curTime <= EndDayTime Then
            lblShift0.Text = "C"
        ElseIf curTime >= NextDayStartTime AndAlso curTime < ShiftA Then
            lblShift0.Text = "C"
        Else
            lblShift0.Text = "G"
        End If

    End Sub
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        btnPrint.Enabled = False

        Dim hid As Single = objcon.get_DextholdID()
        GlobalVariables.HId = hid


        dt = objcon.GetDetails().Tables(0)
        If dt.Rows.Count > 0 Then
            GlobalVariables.extdate = Convert.ToDateTime(dt.Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
            GlobalVariables.exttime = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("HH:mm:ss tt")
            GlobalVariables.Press = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("hh:mm tt")
            GlobalVariables.extshift = Convert.ToString(dt.Rows(0).ItemArray(3))
            GlobalVariables.SelectionTreadCode = Convert.ToString(dt.Rows(0).ItemArray(4))
            GlobalVariables.description = Convert.ToString(dt.Rows(0).ItemArray(5))
            GlobalVariables.Code = Convert.ToString(dt.Rows(0).ItemArray(6))
            GlobalVariables.Qty = Convert.ToString(dt.Rows(0).ItemArray(7))
            GlobalVariables.Location = Convert.ToString(dt.Rows(0).ItemArray(8))
            GlobalVariables.curdate = Convert.ToDateTime(dt.Rows(0).ItemArray(9)).ToString("yyyy/MM/dd HH:mm:ss")
            GlobalVariables.No = Convert.ToString(dt.Rows(0).ItemArray(22))
            GlobalVariables.ItemCode = Convert.ToString(dt.Rows(0).ItemArray(19))
            GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))
            GlobalVariables.Username = Convert.ToString(dt.Rows(0).ItemArray(21))
        End If

        GlobalVariables.Qty = txtQty.Text

        GlobalVariables.HEmpNo = LblOperaor.Text
        GlobalVariables.HEmpName = Lblname.Text
        GlobalVariables.HBy = txtHld.Text
        GlobalVariables.HReason = CboReason.Text
        GlobalVariables.HDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd")
        GlobalVariables.HShift = lblShift0.Text

        If GlobalVariables.HReason <> "" And GlobalVariables.HBy <> "" Then
            objcon.insertTreadHoldEntery()
            objcon.insertTreadHoldStatus()

            ' SMS Alert
            'DsSelection = objcon.GetMobileno()
            'Dim rwcnt As Integer = 0

            'If DsSelection.Tables(0).Rows.Count > 0 Then


            '    For i As Integer = 0 To DsSelection.Tables(0).Rows.Count - 1
            '        Dim dr As DataRow = dt.NewRow()

            '        GlobalVariables.MobNo = Convert.ToString(DsSelection.Tables(0).Rows(i).ItemArray(0))

            '        Dim postData As New StringBuilder
            '        'Dim message As String = "Your password request has been submitted successfully.Sap No"
            '        Dim message As String = "Held up : " & GlobalVariables.HDate & " - " + GlobalVariables.HShift + " - " + GlobalVariables.ItemCode + " - Qty - " + GlobalVariables.Qty + " ( " + GlobalVariables.HReason + " ) " + " Entered By : " + GlobalVariables.HEmpName

            '        Dim createdURL As String = "http://bulksmsalert.in/api/smsapi.aspx?username=apollo&password=apollo&to=" & GlobalVariables.MobNo & "&from=Apollo&message=" + message + ""


            '        Dim wrGETURL As WebRequest
            '        Dim objReader As StreamReader
            '        wrGETURL = WebRequest.Create(createdURL)
            '        Dim proxy As IWebProxy = wrGETURL.Proxy
            '        If proxy IsNot Nothing Then

            '            wrGETURL.UseDefaultCredentials = False
            '            wrGETURL.Proxy = New WebProxy("http://172.19.0.121:8080", False)
            '            wrGETURL.Proxy.Credentials = New NetworkCredential("helpdesk_prmb", "support@atl")
            '        End If

            '        Try
            '            Dim objStream As System.IO.Stream
            '            objStream = wrGETURL.GetResponse().GetResponseStream()
            '            objReader = New StreamReader(objStream)
            '            objReader.Close()
            '        Catch ex As Exception
            '            MessageBox.Show(ex.ToString())

            '        End Try

            '        rwcnt += 1
            '    Next
            'End If

            '' ' objcon.DeleteTread()


            If txtColourCode.Text.StartsWith("SW") Then
                Dim pd1 As New PrintDocument
                AddHandler pd1.PrintPage, AddressOf Me.PrintDocument2_PrintPage
                pd1.PrintController = New StandardPrintController()

                pd1.DefaultPageSettings.Landscape = False
                pd1.PrinterSettings.Copies = "1"

                pd1.Print()
            Else
                Dim pd As New PrintDocument
                AddHandler pd.PrintPage, AddressOf Me.PrintDocument1_PrintPage
                pd.PrintController = New StandardPrintController()

                pd.DefaultPageSettings.Landscape = False
                pd.PrinterSettings.Copies = "1"

                pd.Print()

            End If

            btnPrint.Enabled = False
            txtId.Text = ""
            txtItemCode.Text = ""
            txtColourCode.Text = ""
            txtLTruckNo.Text = ""
            txtLoc.Text = ""
            txtQty.Text = ""
            txtNo.Text = ""
            txtHld.Text = ""

        Else
            MessageBox.Show("Enter All Details...")
            btnPrint.Enabled = True
        End If



    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        btnPrint.Enabled = False
        txtId.Text = ""
        txtItemCode.Text = ""
        txtColourCode.Text = ""
        txtLTruckNo.Text = ""
        txtLoc.Text = ""
        txtQty.Text = ""
        txtNo.Text = ""
        txtHld.Text = ""

    End Sub

    Private Sub btnEnter_Click(sender As Object, e As EventArgs) Handles btnEnter.Click
        If txtId.Text <> "" Then
            btnPrint.Enabled = True
            txtQty.Enabled = True

            GlobalVariables.id = txtId.Text
            dt = objcon.GetHoldDetails().Tables(0)
            If dt.Rows.Count > 0 Then
                txtItemCode.Text = Convert.ToString(dt.Rows(0).ItemArray(4))
                txtColourCode.Text = Convert.ToString(dt.Rows(0).ItemArray(19))
                txtLTruckNo.Text = Convert.ToString(dt.Rows(0).ItemArray(6))
                txtLoc.Text = Convert.ToString(dt.Rows(0).ItemArray(8))
                txtQty.Text = Convert.ToString(dt.Rows(0).ItemArray(7))
                txtNo.Text = Convert.ToString(dt.Rows(0).ItemArray(22))


                'If txtColourCode.Text.StartsWith("SW") Then
                '    GlobalVariables.Defect_Type = "SW"
                'Else
                '    GlobalVariables.Defect_Type = "other"
                'End If

                GlobalVariables.SelectionTreadCode = txtItemCode.Text
                GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))

                ddt = objcon.GetCompound().Tables(0)
                txtLocation.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))
                GlobalVariables.Tyre = txtLocation.Text


            Else
                MessageBox.Show("No Data Found")
                txtId.Text = ""
                btnPrint.Enabled = False
                txtItemCode.Text = ""
                txtColourCode.Text = ""
                txtLTruckNo.Text = ""
                txtLoc.Text = ""
                txtQty.Text = ""
                txtNo.Text = ""
                txtLTruckNo.Text = ""
            End If
        Else
            MessageBox.Show("Enter the Id")
        End If
    End Sub
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage



        ''   Lblcomp.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))



        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.HId, printFontSmall, Brushes.Black, 240, 30)


        ''  e.Graphics.TranslateTransform(0, 200)
        ''  e.Graphics.RotateTransform(If(e.PageSettings.Landscape, 30, 60))

        e.Graphics.RotateTransform(50) 'rotate 45° clockwise
        Dim myfont As New Font("Arial", 80)
        Dim myBrush As New SolidBrush(Color.FromArgb(30, 0, 0, 0))
        e.Graphics.DrawString("HOLD", myfont, myBrush, 110, 0) 'this location is relative to the point of rotation
        e.Graphics.ResetTransform()

        'e.Graphics.RotateTransform(0) 'rotate 45° clockwise
        'Dim myfont1 As New Font("Arial", 150)
        'Dim myBrush1 As New SolidBrush(Color.FromArgb(100, 0, 0, 255))
        'e.Graphics.DrawString("X", myfont, myBrush, 120, 120) 'this location is relative to the point of rotation
        'e.Graphics.ResetTransform()



        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.HId, QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)



        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("HOLD TREAD", printFontHeadHigh, Brushes.Black, 10, 80)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 180, 20)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(GlobalVariables.ItemCode, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("TREAD", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(GlobalVariables.SelectionTreadCode, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(GlobalVariables.Qty, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("E DATE/SHIFT", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(GlobalVariables.extdate & "--" & GlobalVariables.extshift, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("H DATE/SHIFT", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.HDate & "--" & GlobalVariables.HShift, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString(GlobalVariables.HEmpNo, printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.HEmpName & "/" & GlobalVariables.HBy, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("LT NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("TR NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(GlobalVariables.No, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("Rsn:", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(GlobalVariables.HReason, printFont, Brushes.Black, 70, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


        Dim Line5 As New Point(0, 0)
        Dim Line6 As New Point(300, 500)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line5, Line6)

        Dim Line7 As New Point(0, 500)
        Dim Line8 As New Point(300, 0)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line7, Line8)

    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage



        ''   Lblcomp.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))



        Dim printFont As New Font("Bell MT Bold", 12.0F, FontStyle.Bold)
        Dim printFontSmall As New Font("Bell MT Bold", 8.0F, FontStyle.Bold)
        Dim printFontHead As New Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold)
        Dim printFontHeadHigh As New Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold)
        Dim printFontHeadHighest As New Font("Bell MT Bold", 36.0F, FontStyle.Bold)

        e.Graphics.DrawString(GlobalVariables.HId, printFontSmall, Brushes.Black, 240, 30)


        ''  e.Graphics.TranslateTransform(0, 200)
        ''  e.Graphics.RotateTransform(If(e.PageSettings.Landscape, 30, 60))

        e.Graphics.RotateTransform(50) 'rotate 45° clockwise
        Dim myfont As New Font("Arial", 80)
        Dim myBrush As New SolidBrush(Color.FromArgb(30, 0, 0, 0))
        e.Graphics.DrawString("HOLD", myfont, myBrush, 110, 0) 'this location is relative to the point of rotation
        e.Graphics.ResetTransform()

        'e.Graphics.RotateTransform(0) 'rotate 45° clockwise
        'Dim myfont1 As New Font("Arial", 150)
        'Dim myBrush1 As New SolidBrush(Color.FromArgb(100, 0, 0, 255))
        'e.Graphics.DrawString("X", myfont, myBrush, 120, 120) 'this location is relative to the point of rotation
        'e.Graphics.ResetTransform()



        Dim qrGenerator As QRCodeGenerator = New QRCodeGenerator()
        Dim qrCode As QRCodeGenerator.QRCode = qrGenerator.CreateQrCode(GlobalVariables.HId, QRCodeGenerator.ECCLevel.H)


        Dim bitmap As Bitmap = qrCode.GetGraphic(2.5)
        Dim ms As MemoryStream = New MemoryStream()
        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png)



        Dim blackPen As New Pen(Color.Black, 3)

        ' Create points that define line.
        Dim x As Integer = 15
        Dim y As Integer = 130
        Dim width As Integer = 280
        Dim height As Integer = 40

        ' Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)



        e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500)
        e.Graphics.DrawString("HOLD SW", printFontHeadHigh, Brushes.Black, 10, 80)
        ' e.Graphics.DrawString("FT-01-B5:03", printFontSmall, Brushes.Black, 10, 480)


        e.Graphics.DrawImage(bitmap, 180, 20)
        e.Graphics.DrawString("CODE", printFont, Brushes.Black, 20, 140)
        '   e.Graphics.DrawString("CODE", printFont, Brushes.Black, 10, 140)

        e.Graphics.DrawString(GlobalVariables.ItemCode, printFont, Brushes.Black, 130, 140)

        x = 15
        y = 170
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("SIDEWALL", printFont, Brushes.Black, 20, 180)
        e.Graphics.DrawString(GlobalVariables.SelectionTreadCode, printFont, Brushes.Black, 130, 180)
        x = 15
        y = 210
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220)
        e.Graphics.DrawString(GlobalVariables.Qty, printFont, Brushes.Black, 130, 220)
        e.Graphics.DrawString(GlobalVariables.DualSelection, printFont, Brushes.Black, 210, 220)

        x = 15
        y = 250
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("E DATE/SHIFT", printFont, Brushes.Black, 20, 260)
        e.Graphics.DrawString(GlobalVariables.extdate & "--" & GlobalVariables.extshift, printFont, Brushes.Black, 130, 260)

        x = 15
        y = 290
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("LOCATION", printFont, Brushes.Black, 20, 300)
        e.Graphics.DrawString(GlobalVariables.Location, printFont, Brushes.Black, 130, 300)
        x = 15
        y = 330
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("H DATE/SHIFT", printFont, Brushes.Black, 20, 340)
        e.Graphics.DrawString(GlobalVariables.HDate & "--" & GlobalVariables.HShift, printFont, Brushes.Black, 130, 340)
        x = 15
        y = 370
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString(GlobalVariables.HEmpNo, printFont, Brushes.Black, 20, 380)
        e.Graphics.DrawString(GlobalVariables.HEmpName & "/" & GlobalVariables.HBy, printFont, Brushes.Black, 130, 380)

        x = 15
        y = 410
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)


        e.Graphics.DrawString("SW NO : ", printFont, Brushes.Black, 20, 420)
        e.Graphics.DrawString("SL NO : ", printFont, Brushes.Black, 140, 420)
        e.Graphics.DrawString(GlobalVariables.Code, printFont, Brushes.Black, 100, 420)
        e.Graphics.DrawString(GlobalVariables.No, printFont, Brushes.Black, 220, 420)

        x = 15
        y = 450
        e.Graphics.DrawRectangle(blackPen, x, y, width, height)

        e.Graphics.DrawString("Rsn:", printFont, Brushes.Black, 20, 460)
        e.Graphics.DrawString(GlobalVariables.HReason, printFont, Brushes.Black, 70, 460)

        x = 15
        y = 450

        e.Graphics.DrawRectangle(blackPen, x, y, width, height)




        Dim Line1 As New Point(130, 130)
        Dim Line2 As New Point(130, 410)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line1, Line2)

        Dim Line3 As New Point(210, 210)
        Dim Line4 As New Point(210, 250)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line3, Line4)


        Dim Line5 As New Point(0, 0)
        Dim Line6 As New Point(300, 500)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line5, Line6)

        Dim Line7 As New Point(0, 500)
        Dim Line8 As New Point(300, 0)

        ' Draw line to screen.
        e.Graphics.DrawLine(blackPen, Line7, Line8)

    End Sub

End Class