﻿Imports Excel = Microsoft.Office.Interop.Excel
Public Class LeafTruckRpt

    Dim DTSpec As DataTable
    Dim objcon As New Connections
    Public ds As New DataSet()
    ' Dim DtLT As DataTable
    Dim rwcnt As Integer = 0
    'Public dt As New DataTable()
    Dim excelLocation As String
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Dim Search As String

    Private Sub RBEmptyLeafTruck_CheckedChanged(sender As Object, e As EventArgs) Handles RBEmptyLeafTruck.CheckedChanged

        rwcnt = 0
        Search = "Empty"
        CmbShift.Visible = False
        DTPSelectedDate.Visible = False
        'DtpToDate.Visible = False

    End Sub

    Private Sub RBTBRequest_CheckedChanged(sender As Object, e As EventArgs) Handles RBTBRequest.CheckedChanged

        rwcnt = 0
        'DataGridView1.Visible = True
        CmbShift.Visible = True
        DTPSelectedDate.Visible = True
        Search = "TBWithRequest"
        'DtpToDate.Visible = False

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged

        rwcnt = 0
        ' DataGridView1.Visible = True
        CmbShift.Visible = True
        DTPSelectedDate.Visible = True
        Search = "TBWithoutRequest"
        ' DtpToDate.Visible = False

    End Sub

    Private Sub RBScrap_CheckedChanged(sender As Object, e As EventArgs) Handles RBScrap.CheckedChanged

        rwcnt = 0
        ' DataGridView1.Visible = True
        CmbShift.Visible = False
        'DTPSelectedDate.Visible = True
        ' DtpToDate.Visible = True




        Search = "Scrap"

    End Sub

    Private Sub BtnExport_Click(sender As Object, e As EventArgs) Handles BtnExport.Click
        If DataGridView1.Rows.Count <= 2 Then
            MsgBox("Nothing to Export")
            Exit Sub

        End If
        Dim saveDlg As New System.Windows.Forms.SaveFileDialog()

        saveDlg.InitialDirectory = "C:\"
        saveDlg.Filter = "Excel files (*.xlsx)|*.xlsx"
        saveDlg.FilterIndex = 0
        saveDlg.RestoreDirectory = True
        saveDlg.Title = "Export Excel File To"

        If saveDlg.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            workbook = APP.Workbooks.Open("c:\Temp\File.xlsx")
            worksheet = workbook.Worksheets("Sheet1")

            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).Merge()
            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).VerticalAlignment = Excel.Constants.xlCenter
            worksheet.Cells(1, 1).Value = "   Leaf Truck Report   "
            Dim columnsCount As Integer = DataGridView1.Columns.Count
            For Each column In DataGridView1.Columns
                worksheet.Cells(2, column.Index + 1).Value = column.Name
                worksheet.Columns.AutoFit()
            Next
            'Export Header Name End


            'Export Each Row Start
            For i As Integer = 0 To DataGridView1.Rows.Count - 2
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    worksheet.Cells(i + 3, columnIndex + 1).Value = DataGridView1.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next
            'Export Each Row End

            excelLocation = saveDlg.FileName
            workbook.SaveCopyAs(excelLocation)
            workbook.Saved = True
            workbook.Close(True, 0, 0)
            APP.Quit()

            MessageBox.Show("Exporting is completed..")
        End If
    End Sub

    Private Sub LeafTruckRpt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CmbShift.Visible = False
        DTPSelectedDate.Visible = False
        'DtpToDate.Visible = False

    End Sub



    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        DataGridView1.Columns.Clear()
        ds.Tables.Clear()
        ' DataGridView1.Visible = True
        ' CmbShift.Visible = True
        ' DTPSelectedDate.Visible = True
        rwcnt = 0
        If Search = "Empty" Then
            Dim dt1 As New DataTable()

            ' Dim ds1 As DataSet = Nothing
            dt1.Columns.Add("Truck No", GetType(String))
            dt1.Columns.Add("Location", GetType(String))
            'ds1.Clear()
            ds = objcon.GETEmptyLeafTRuck()

            If ds.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    Dim dr3 As DataRow = dt1.NewRow()

                    dt1.Rows.Add(dr3)
                    dt1.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                    dt1.Rows(rwcnt)(1) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1))
                    rwcnt += 1


                Next
                DataGridView1.DataSource = dt1
                DataGridView1.Visible = True


            Else
                MessageBox.Show("No data Found !!!")
            End If

        End If
        If Search = "Scrap" Then
            ' DtpToDate.Visible = True

            Dim selected As DateTime = DTPSelectedDate.Text
            GlobalVariables.TbWithoutReqStart = selected

            '  Dim todate As DateTime = DtpToDate.Text
            ' GlobalVariables.ToDate = todate

            Dim dt2 As New DataTable()


            '      se.[truckno], zm.Zone_Name, se.[scrp_date], se.[scrp_time], se.[scrp_shift], se.[treadcode], se.[colorcode], se.[description] 

            ', se.[treads]  , se.[prod_date]  , se.[prod_time] , se.[prod_shift] , se.[pkdextid]  , se.[Dual_Ext]

            ' ds.Clear()
            ' Dim ds2 As DataSet = Nothing
            dt2.Columns.Add("Truck No", GetType(String))
            dt2.Columns.Add("Location", GetType(String))
            'dt2.Columns.Add("Scrap Date", GetType(String))
            'dt2.Columns.Add("Scrap Time", GetType(String))
            'dt2.Columns.Add("Shift", GetType(String))
            'dt2.Columns.Add("Tread", GetType(String))
            'dt2.Columns.Add("Color Code", GetType(String))
            'dt2.Columns.Add("Desc", GetType(String))
            'dt2.Columns.Add("Treads", GetType(String))
            'dt2.Columns.Add("Prod Date", GetType(String))
            'dt2.Columns.Add("Prod Time", GetType(String))
            'dt2.Columns.Add("Prod Shift", GetType(String))
            'dt2.Columns.Add("pkdextid", GetType(String))
            'dt2.Columns.Add("Dual", GetType(String))
            'ds2.Clear()
            ds = objcon.GETScrapLeafTruck()

            If ds.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                    Dim dr2 As DataRow = dt2.NewRow()

                    dt2.Rows.Add(dr2)
                    dt2.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                    dt2.Rows(rwcnt)(1) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                    'dt2.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                    'dt2.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                    'dt2.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(8) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(8))
                    'dt2.Rows(rwcnt)(9) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(9)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(10) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(10))
                    'dt2.Rows(rwcnt)(11) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(11)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(12) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(12))
                    'dt2.Rows(rwcnt)(13) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(13)) ''.ToString("dd/MM/yyyy")
                    'dt2.Rows(rwcnt)(14) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(14))
                    'dt2.Rows(rwcnt)(15) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(15)) ''.ToString("dd/MM/yyyy")

                    rwcnt += 1


                Next
                DataGridView1.DataSource = dt2
                DataGridView1.Visible = True

            Else
                MessageBox.Show("No data Found !!!")
            End If

        End If
        If Search = "TBWithoutRequest" Then
            If CmbShift.Text = "" Then
                MessageBox.Show("Please Select Shift!!!")
            Else
                Dim dt3 As New DataTable()
                'ds.Clear()
                '  Dim ds3 As DataSet = Nothing
                ' ds.Tables.

                'Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime, lastDay As DateTime
                ' Dim now As DateTime = DateTime.Now
                Dim selected As DateTime = DTPSelectedDate.Text

                Dim startDate As String
                '  Dim startTime As TimeSpan
                ' Dim endTime As TimeSpan
                Dim endDate As String

                If CmbShift.Text = "A" Then
                    startDate = selected.ToString("yyyy-MM-dd") + " 06:01:00"
                    endDate = selected.ToString("yyyy-MM-dd") + " 14:01:00"

                ElseIf CmbShift.Text = "B" Then
                    startDate = selected.ToString("yyyy-MM-dd") + " 14:01:00"
                    endDate = selected.ToString("yyyy-MM-dd") + " 22:01:00"
                ElseIf CmbShift.Text = "C" Then
                    startDate = selected.ToString("yyyy-MM-dd") + " 22:01:00"
                    endDate = selected.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00"

                End If
                GlobalVariables.TbWithoutReqStart = startDate

                '[Dual_Ext] ,[TRUCK_NO] ,[EXT_DATE] ,[PKDEXTID], [Tread_Code] 
                GlobalVariables.TbWithoutReqend = endDate
                dt3.Columns.Add("Truck No", GetType(String))
                dt3.Columns.Add("Dual_Ext", GetType(String))
                dt3.Columns.Add("EXT_DATE", GetType(String))
                dt3.Columns.Add("PKDEXTID", GetType(String))
                dt3.Columns.Add("Color Code", GetType(String))
                dt3.Columns.Add("TREADCODE", GetType(String))
                dt3.Columns.Add("Time", GetType(String))
                'ds3.Clear()

                ds = objcon.GETTakenWithoutRequest()

                If ds.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                        Dim dr As DataRow = dt3.NewRow()

                        dt3.Rows.Add(dr)
                        dt3.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                        dt3.Rows(rwcnt)(1) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1))
                        dt3.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                        dt3.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                        dt3.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                        dt3.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                        dt3.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))

                        rwcnt += 1


                    Next
                    DataGridView1.DataSource = dt3
                    DataGridView1.Visible = True

                Else
                    MessageBox.Show("No data Found !!!")
                End If
            End If
        End If
        If Search = "TBWithRequest" Then

            If CmbShift.Text = "" Then
                MessageBox.Show("Please Select Shift!!!")
            Else
                Dim dt4 As New DataTable()
                'ds.Clear()
                'Dim ds4 As DataSet = Nothing
                Dim selected As DateTime = DTPSelectedDate.Text

                Dim startDate As String
                '  Dim startTime As TimeSpan
                ' Dim endTime As TimeSpan
                Dim endDate As String

                If CmbShift.Text = "A" Then
                    startDate = selected.ToString("dd/MM/yyyy") + " 06:01:00"
                    endDate = selected.ToString("dd/MM/yyyy") + " 14:01:00"

                ElseIf CmbShift.Text = "B" Then
                    startDate = selected.ToString("dd/MM/yyyy") + " 14:01:00"
                    endDate = selected.ToString("dd/MM/yyyy") + " 22:01:00"
                ElseIf CmbShift.Text = "C" Then
                    startDate = selected.ToString("dd/MM/yyyy") + " 22:01:00"
                    endDate = selected.AddDays(1).ToString("dd/MM/yyyy") + " 06:00:00"

                End If
                startDate = startDate.Replace("-", "/")
                endDate = endDate.Replace("-", "/")
                GlobalVariables.TbWithoutReqStart = startDate
                GlobalVariables.TbWithoutReqend = endDate

                dt4.Columns.Add("Truck No", GetType(String))
                dt4.Columns.Add("Dual_Ext", GetType(String))
                dt4.Columns.Add("Prod_Date", GetType(String))
                dt4.Columns.Add("pkdextid", GetType(String))
                dt4.Columns.Add("ColorCode", GetType(String))
                dt4.Columns.Add("TreadCode", GetType(String))
                dt4.Columns.Add("RequestTime", GetType(String))
                dt4.Columns.Add("Machine No", GetType(String))
                'ds4.Clear()
                ds = objcon.GETTakenOnRequest()

                If ds.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                        Dim dr As DataRow = dt4.NewRow()

                        dt4.Rows.Add(dr)
                        dt4.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                        dt4.Rows(rwcnt)(1) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1))
                        dt4.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                        dt4.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                        dt4.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                        dt4.Rows(rwcnt)(5) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(5))
                        dt4.Rows(rwcnt)(6) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(6))
                        dt4.Rows(rwcnt)(7) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(7))
                        rwcnt += 1


                    Next
                    DataGridView1.DataSource = dt4
                    DataGridView1.Visible = True


                Else
                    MessageBox.Show("No data Found !!!")
                End If
            End If
        End If


    End Sub

    Private Sub CmbShift_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CmbShift.SelectedIndexChanged

    End Sub
End Class