﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmManualSchedule
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboMachine = New System.Windows.Forms.ComboBox()
        Me.TDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Fdate = New System.Windows.Forms.DateTimePicker()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.BtnSubmit = New System.Windows.Forms.Button()
        Me.DGSchSelection = New System.Windows.Forms.DataGridView()
        Me.SLNO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Priority = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColorCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Schedule = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Compound = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.comp2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.comp3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProductionEntry = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.BackColor = System.Drawing.Color.LightPink
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.ForeColor = System.Drawing.Color.Maroon
        Me.lblCode.Location = New System.Drawing.Point(411, 9)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(270, 47)
        Me.lblCode.TabIndex = 81
        Me.lblCode.Text = "ALL SCHEDULE"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Coral
        Me.Label6.Location = New System.Drawing.Point(35, 92)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 24)
        Me.Label6.TabIndex = 107
        Me.Label6.Text = "From Date"
        '
        'cboMachine
        '
        Me.cboMachine.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMachine.FormattingEnabled = True
        Me.cboMachine.Items.AddRange(New Object() {"dual1", "dual2"})
        Me.cboMachine.Location = New System.Drawing.Point(693, 96)
        Me.cboMachine.Name = "cboMachine"
        Me.cboMachine.Size = New System.Drawing.Size(89, 28)
        Me.cboMachine.TabIndex = 106
        '
        'TDate
        '
        Me.TDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TDate.Location = New System.Drawing.Point(464, 94)
        Me.TDate.Name = "TDate"
        Me.TDate.Size = New System.Drawing.Size(193, 26)
        Me.TDate.TabIndex = 110
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Coral
        Me.Label1.Location = New System.Drawing.Point(362, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 24)
        Me.Label1.TabIndex = 109
        Me.Label1.Text = "To Date"
        '
        'Fdate
        '
        Me.Fdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fdate.Location = New System.Drawing.Point(148, 92)
        Me.Fdate.Name = "Fdate"
        Me.Fdate.Size = New System.Drawing.Size(193, 26)
        Me.Fdate.TabIndex = 108
        '
        'BtnCancel
        '
        Me.BtnCancel.BackColor = System.Drawing.Color.Tomato
        Me.BtnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCancel.Location = New System.Drawing.Point(917, 92)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(81, 42)
        Me.BtnCancel.TabIndex = 121
        Me.BtnCancel.Text = "CANCEL"
        Me.BtnCancel.UseVisualStyleBackColor = False
        '
        'BtnSubmit
        '
        Me.BtnSubmit.BackColor = System.Drawing.Color.Tomato
        Me.BtnSubmit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSubmit.Location = New System.Drawing.Point(826, 92)
        Me.BtnSubmit.Name = "BtnSubmit"
        Me.BtnSubmit.Size = New System.Drawing.Size(85, 42)
        Me.BtnSubmit.TabIndex = 120
        Me.BtnSubmit.Text = "SUBMIT"
        Me.BtnSubmit.UseVisualStyleBackColor = False
        '
        'DGSchSelection
        '
        Me.DGSchSelection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Peru
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGSchSelection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGSchSelection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGSchSelection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SLNO, Me.Code, Me.Description, Me.Priority, Me.ColorCode, Me.Schedule, Me.Compound, Me.comp2, Me.comp3, Me.ProductionEntry})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGSchSelection.DefaultCellStyle = DataGridViewCellStyle4
        Me.DGSchSelection.Location = New System.Drawing.Point(12, 140)
        Me.DGSchSelection.Name = "DGSchSelection"
        Me.DGSchSelection.RowTemplate.Height = 30
        Me.DGSchSelection.Size = New System.Drawing.Size(1125, 531)
        Me.DGSchSelection.TabIndex = 122
        '
        'SLNO
        '
        Me.SLNO.HeaderText = "SL No"
        Me.SLNO.Name = "SLNO"
        Me.SLNO.Visible = False
        '
        'Code
        '
        Me.Code.HeaderText = "Tread Code"
        Me.Code.Name = "Code"
        Me.Code.Width = 200
        '
        'Description
        '
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.Width = 350
        '
        'Priority
        '
        Me.Priority.HeaderText = "Priority"
        Me.Priority.Name = "Priority"
        '
        'ColorCode
        '
        Me.ColorCode.HeaderText = "Color Code"
        Me.ColorCode.Name = "ColorCode"
        Me.ColorCode.Width = 150
        '
        'Schedule
        '
        Me.Schedule.HeaderText = "Schedule"
        Me.Schedule.Name = "Schedule"
        Me.Schedule.Width = 120
        '
        'Compound
        '
        Me.Compound.HeaderText = "Compound"
        Me.Compound.Name = "Compound"
        Me.Compound.Visible = False
        '
        'comp2
        '
        Me.comp2.HeaderText = "comp2"
        Me.comp2.Name = "comp2"
        Me.comp2.Visible = False
        '
        'comp3
        '
        Me.comp3.HeaderText = "comp3"
        Me.comp3.Name = "comp3"
        Me.comp3.Visible = False
        '
        'ProductionEntry
        '
        Me.ProductionEntry.HeaderText = "Production Entry"
        Me.ProductionEntry.Name = "ProductionEntry"
        Me.ProductionEntry.Text = "SELECT"
        Me.ProductionEntry.UseColumnTextForButtonValue = True
        Me.ProductionEntry.Width = 150
        '
        'FrmManualSchedule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1222, 733)
        Me.Controls.Add(Me.DGSchSelection)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.BtnSubmit)
        Me.Controls.Add(Me.TDate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Fdate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboMachine)
        Me.Controls.Add(Me.lblCode)
        Me.Name = "FrmManualSchedule"
        Me.Text = "FrmManualSchedule"
        CType(Me.DGSchSelection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboMachine As System.Windows.Forms.ComboBox
    Friend WithEvents TDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Fdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnCancel As System.Windows.Forms.Button
    Friend WithEvents BtnSubmit As System.Windows.Forms.Button
    Friend WithEvents DGSchSelection As System.Windows.Forms.DataGridView
    Friend WithEvents SLNO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Priority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColorCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Schedule As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Compound As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents comp2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents comp3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProductionEntry As System.Windows.Forms.DataGridViewButtonColumn
End Class
