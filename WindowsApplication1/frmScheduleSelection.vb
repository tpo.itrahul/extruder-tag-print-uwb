﻿Public Class frmScheduleSelection
    Dim DT As DataTable
    Dim dt_TreadList As New DataTable
    Dim dt_TreadAboveSchedule As New DataTable
    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String

    Private Sub frmScheduleSelection_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BindCurrentDateandShift()
        DT = New DataTable
        '   lblDate.Text = Now
        Dim Dtt As Date = Convert.ToDateTime(lblDate.Text)


        If DateTime.Now > Convert.ToDateTime("12:00:00 AM") And DateTime.Now < Convert.ToDateTime("06:00:00 AM") Then

            Dim DT As DateTime = DateTime.Now
            lblDate.Text = DT.AddDays(-1).ToString("yyyy/MM/dd")


        Else

            lblDate.Text = DateTime.Now.ToString("yyyy/MM/dd")
        End If



        If GlobalVariables.DualShift = "C" Then

            'DualExactDate = Dtt.ToString("yyyy/MM/dd")
            DualExactDate = lblDate.Text
            DualExactShift = lblShift.Text
        Else
            If lblShift.Text = "A" Then
                DualExactShift = "C"
                DualExactDate = Dtt.AddDays(-1).ToString("yyyy/MM/dd")

            ElseIf lblShift.Text = "B" Then
                DualExactDate = Dtt.ToString("yyyy/MM/dd")
                DualExactShift = "A"
            ElseIf lblShift.Text = "C" Then
                DualExactDate = Dtt.AddDays(-1).ToString("yyyy/MM/dd")
                DualExactShift = "B"
            End If
        End If
        GlobalVariables.DualExactDate = DualExactDate.ToString("yyyy/MM/dd")
        GlobalVariables.DualExactShift = DualExactShift

        DT = objcon.GetDualScheduleData().Tables(0)

        If DT.Rows.Count > 0 Then

            DGSchSelection.AutoGenerateColumns = False
            DGSchSelection.Columns(1).DataPropertyName = "ext_code"
            DGSchSelection.Columns(2).DataPropertyName = "tread_name"
            DGSchSelection.Columns(3).DataPropertyName = "priority_No"
            DGSchSelection.Columns(4).DataPropertyName = "Tread_Code"
            DGSchSelection.Columns(5).DataPropertyName = "sch_value"
            DGSchSelection.Columns(6).DataPropertyName = "Comp_Cap"
            DGSchSelection.Columns(7).DataPropertyName = "Comp_Base"
            DGSchSelection.Columns(8).DataPropertyName = "Comp_Cushion"

            DGSchSelection.DataSource = DT

        End If

        dt_TreadList = objcon.Get_Tagged_TreadList(DualExactShift, DualExactDate.ToString("yyyy/MM/dd"))

        For i = 0 To dt_TreadList.Rows.Count - 1

            For j = 0 To DGSchSelection.Rows.Count - 1
                Dim str As String = DGSchSelection.Rows(j).Cells(1).Value()
                Dim str1 As String = dt_TreadList.Rows(i)(0).ToString()
                If DGSchSelection.Rows(j).Cells(1).Value() = dt_TreadList.Rows(i)(0).ToString() Then
                    DGSchSelection.Rows(j).DefaultCellStyle.ForeColor = Color.Blue
                    Exit For
                End If
            Next

        Next

        dt_TreadAboveSchedule = objcon.Get_TreadList_AboveScheduled(DualExactShift, DualExactDate.ToString("yyyy/MM/dd"))
        For i = 0 To dt_TreadAboveSchedule.Rows.Count - 1

            For j = 0 To DGSchSelection.Rows.Count - 1
                Dim str As String = DGSchSelection.Rows(j).Cells(1).Value()
                Dim str1 As String = dt_TreadAboveSchedule.Rows(i)(0).ToString()
                If DGSchSelection.Rows(j).Cells(1).Value() = dt_TreadAboveSchedule.Rows(i)(0).ToString() Then
                    DGSchSelection.Rows(j).DefaultCellStyle.ForeColor = Color.LimeGreen
                    Exit For
                End If
            Next

        Next

        Label19.Text = GlobalVariables.DualSelection
        Label1.Text = GlobalVariables.DualExactDate
        Label2.Text = GlobalVariables.DualExactShift

    End Sub

    Private Sub BindCurrentDateandShift()
        Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime, lastDay As DateTime
        Dim now As DateTime = DateTime.Now
        Dim curTime As DateTime = DateTime.Now
        ShiftA = Convert.ToDateTime("06:01:00 AM")
        ShiftB = Convert.ToDateTime("02:01:00 PM")
        ShiftC = Convert.ToDateTime("10:01:00 PM")
        EndDayTime = Convert.ToDateTime("11:59:55 PM")
        NextDayStartTime = Convert.ToDateTime("12:00:00 AM")
        If curTime >= ShiftA AndAlso curTime < ShiftB Then

            lblShift.Text = "A"
        ElseIf curTime >= ShiftB AndAlso curTime < ShiftC Then
            lblShift.Text = "B"
        ElseIf curTime >= ShiftC AndAlso curTime <= EndDayTime Then
            lblShift.Text = "C"
        ElseIf curTime >= NextDayStartTime AndAlso curTime < ShiftA Then
            lblShift.Text = "C"
        Else
            lblShift.Text = "G"
        End If
        lblDate.Text = now

    End Sub

    Private Sub DGSchSelection_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGSchSelection.CellContentClick
        If e.ColumnIndex = 9 Then
            GlobalVariables.SelectionTreadCode = DGSchSelection.Rows(e.RowIndex).Cells(1).Value()
            GlobalVariables.ItemCode = DGSchSelection.Rows(e.RowIndex).Cells(4).Value()
            GlobalVariables.description = DGSchSelection.Rows(e.RowIndex).Cells(2).Value()
            GlobalVariables.Splicer1 = DGSchSelection.Rows(e.RowIndex).Cells(6).Value()
            GlobalVariables.Splicer2 = DGSchSelection.Rows(e.RowIndex).Cells(7).Value()
            GlobalVariables.Splicer3 = DGSchSelection.Rows(e.RowIndex).Cells(8).Value()
            lblTime.Show()

        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Me.Close()
        frmDualSelecion.Show()
    End Sub
End Class