﻿Public Class DeleteDetails

    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String
    Private Sub DeleteDetails_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblSect.Text = GlobalVariables.DualSelection
        DateLabel.Text = GlobalVariables.DualExactDate
        ShiftLabel.Text = GlobalVariables.DualExactShift


        GlobalVariables.CalPRDate = DateLabel.Text
        GlobalVariables.CalPRShift = ShiftLabel.Text
        GridLoad()

    End Sub
    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

     
        Me.Close()

        lblTime.CboLocation.Items.Clear()
        ' lblTime.cboCode.Items.Clear()
        If lblTime.txtColourCode.Text.StartsWith("SW") Then
            lblTime.CodeLoadSW()
        Else
            lblTime.CodeLoad()
        End If

        lblTime.LocationLoad()
        lblTime.Show()
    End Sub

    Private Sub cboCalShift_SelectedIndexChanged(sender As Object, e As EventArgs)
        GlobalVariables.CalPRShift = cboCalShift.Text
    End Sub

  
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        GlobalVariables.CalPRDate = DateLabel.Text
        GlobalVariables.CalPRShift = ShiftLabel.Text
        GridLoad()
     
    End Sub
    Private Sub DGSchSelection_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGSchSelection.CellContentClick
        If e.ColumnIndex = 9 Then
            GlobalVariables.id = DGSchSelection.Rows(e.RowIndex).Cells(0).Value()
            GlobalVariables.Code = DGSchSelection.Rows(e.RowIndex).Cells(4).Value()
            GlobalVariables.Location = DGSchSelection.Rows(e.RowIndex).Cells(6).Value()
            GlobalVariables.SerialNo = DGSchSelection.Rows(e.RowIndex).Cells(5).Value()


            If GlobalVariables.Code.StartsWith("SW") Then
                GlobalVariables.Defect_Type = "SW"
            Else
                GlobalVariables.Defect_Type = "other"
            End If
            Dim DS As DataSet

            DS = objcon.GetDetails()
            If DS.Tables(0).Rows.Count > 0 Then
                GlobalVariables.pkdextid = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(0))
                GlobalVariables.proddate = Convert.ToDateTime(DS.Tables(0).Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
                GlobalVariables.prodtime = Convert.ToDateTime(DS.Tables(0).Rows(0).ItemArray(2)).ToString("HH:mm:ss")
                GlobalVariables.prodshift = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(3))
                GlobalVariables.TreadCode = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(4))
                GlobalVariables.colourcode = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(19))
                GlobalVariables.description = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(5))
                GlobalVariables.truckno = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(6))
                GlobalVariables.oldLocation = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(8))
                GlobalVariables.treads = Convert.ToString(DS.Tables(0).Rows(0).ItemArray(7))
                GlobalVariables.CurrentDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy/MM/dd")
                GlobalVariables.timevalue = Convert.ToDateTime(DateTime.Now).ToString("HH:mm:ss")
                GlobalVariables.shiftTime = ShiftLabel.Text
                GlobalVariables.T1 = lblSect.Text
                GlobalVariables.Bias = GlobalVariables.Username
                objcon.insertDeleteEntry()
            End If




            objcon.DeleteTread()
            objcon.TrueUpdateLocation()


            If GlobalVariables.Code.StartsWith("SW") Then
                GlobalVariables.Defect_Type = "SW"
                objcon.TrueUpdateLeaftruckSW()
            Else
                GlobalVariables.Defect_Type = "other"
                objcon.TrueUpdateLeaftruck()
            End If

            GridLoad()
        End If
    End Sub
    Private Sub GridLoad()
        GlobalVariables.CalPRDate = DateLabel.Text
        GlobalVariables.CalPRShift = ShiftLabel.Text
        Dim DT As DataTable
        DT = objcon.GetDeleteData().Tables(0)
        If DT.Rows.Count > 0 Then
            DGSchSelection.AutoGenerateColumns = False
            DGSchSelection.Columns(0).DataPropertyName = "PKDEXTID"
            DGSchSelection.Columns(1).DataPropertyName = "EXT_DATE"
            DGSchSelection.Columns(2).DataPropertyName = "SHIFT"
            DGSchSelection.Columns(3).DataPropertyName = "TREADCODE"
            DGSchSelection.Columns(4).DataPropertyName = "Tread_Code"
            DGSchSelection.Columns(5).DataPropertyName = "LEAF_TRUCKNO"
            DGSchSelection.Columns(6).DataPropertyName = "LOCATION"
            DGSchSelection.Columns(7).DataPropertyName = "TREADS"
            DGSchSelection.Columns(8).DataPropertyName = "TRUCK_NO"

            DGSchSelection.DataSource = DT
        End If
    End Sub

   

End Class