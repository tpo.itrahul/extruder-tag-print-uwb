﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LeafTruckRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.RBEmptyLeafTruck = New System.Windows.Forms.RadioButton()
        Me.RBScrap = New System.Windows.Forms.RadioButton()
        Me.RBTBRequest = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.BtnExport = New System.Windows.Forms.Button()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.DTPSelectedDate = New System.Windows.Forms.DateTimePicker()
        Me.CmbShift = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Indigo
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 126)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Indigo
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView1.Size = New System.Drawing.Size(935, 473)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.Control
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Navy
        Me.TextBox1.Location = New System.Drawing.Point(332, 14)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(312, 22)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = " LEAF TRUCK REPORT"
        '
        'RBEmptyLeafTruck
        '
        Me.RBEmptyLeafTruck.AutoSize = True
        Me.RBEmptyLeafTruck.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RBEmptyLeafTruck.ForeColor = System.Drawing.Color.Indigo
        Me.RBEmptyLeafTruck.Location = New System.Drawing.Point(24, 46)
        Me.RBEmptyLeafTruck.Name = "RBEmptyLeafTruck"
        Me.RBEmptyLeafTruck.Size = New System.Drawing.Size(176, 24)
        Me.RBEmptyLeafTruck.TabIndex = 8
        Me.RBEmptyLeafTruck.TabStop = True
        Me.RBEmptyLeafTruck.Text = "Empty Leaf Trucks"
        Me.RBEmptyLeafTruck.UseVisualStyleBackColor = True
        '
        'RBScrap
        '
        Me.RBScrap.AutoSize = True
        Me.RBScrap.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RBScrap.ForeColor = System.Drawing.Color.Indigo
        Me.RBScrap.Location = New System.Drawing.Point(24, 74)
        Me.RBScrap.Name = "RBScrap"
        Me.RBScrap.Size = New System.Drawing.Size(74, 24)
        Me.RBScrap.TabIndex = 9
        Me.RBScrap.TabStop = True
        Me.RBScrap.Text = "Scrap"
        Me.RBScrap.UseVisualStyleBackColor = True
        '
        'RBTBRequest
        '
        Me.RBTBRequest.AutoSize = True
        Me.RBTBRequest.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RBTBRequest.ForeColor = System.Drawing.Color.Indigo
        Me.RBTBRequest.Location = New System.Drawing.Point(231, 46)
        Me.RBTBRequest.Name = "RBTBRequest"
        Me.RBTBRequest.Size = New System.Drawing.Size(226, 24)
        Me.RBTBRequest.TabIndex = 10
        Me.RBTBRequest.TabStop = True
        Me.RBTBRequest.Text = "Taken To TB on Request"
        Me.RBTBRequest.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton1.ForeColor = System.Drawing.Color.Indigo
        Me.RadioButton1.Location = New System.Drawing.Point(231, 76)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(268, 24)
        Me.RadioButton1.TabIndex = 11
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Taken To TB Without Request"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'BtnExport
        '
        Me.BtnExport.BackColor = System.Drawing.Color.Indigo
        Me.BtnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnExport.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BtnExport.Location = New System.Drawing.Point(703, 77)
        Me.BtnExport.Name = "BtnExport"
        Me.BtnExport.Size = New System.Drawing.Size(122, 21)
        Me.BtnExport.TabIndex = 12
        Me.BtnExport.Text = "Export"
        Me.BtnExport.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnExport.UseVisualStyleBackColor = False
        '
        'BtnSearch
        '
        Me.BtnSearch.BackColor = System.Drawing.Color.Indigo
        Me.BtnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BtnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSearch.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BtnSearch.Location = New System.Drawing.Point(548, 77)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(137, 21)
        Me.BtnSearch.TabIndex = 14
        Me.BtnSearch.Text = "Show"
        Me.BtnSearch.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnSearch.UseVisualStyleBackColor = False
        '
        'DTPSelectedDate
        '
        Me.DTPSelectedDate.Location = New System.Drawing.Point(548, 45)
        Me.DTPSelectedDate.Name = "DTPSelectedDate"
        Me.DTPSelectedDate.Size = New System.Drawing.Size(136, 20)
        Me.DTPSelectedDate.TabIndex = 15
        '
        'CmbShift
        '
        Me.CmbShift.FormattingEnabled = True
        Me.CmbShift.Items.AddRange(New Object() {"A", "B", "C"})
        Me.CmbShift.Location = New System.Drawing.Point(704, 45)
        Me.CmbShift.Name = "CmbShift"
        Me.CmbShift.Size = New System.Drawing.Size(121, 21)
        Me.CmbShift.TabIndex = 16
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.CmbShift)
        Me.Panel1.Controls.Add(Me.RBScrap)
        Me.Panel1.Controls.Add(Me.DTPSelectedDate)
        Me.Panel1.Controls.Add(Me.RBEmptyLeafTruck)
        Me.Panel1.Controls.Add(Me.BtnSearch)
        Me.Panel1.Controls.Add(Me.BtnExport)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Controls.Add(Me.RBTBRequest)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, -2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(935, 115)
        Me.Panel1.TabIndex = 17
        '
        'LeafTruckRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(959, 611)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "LeafTruckRpt"
        Me.Text = "LeafTruckRpt"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents RBEmptyLeafTruck As RadioButton
    Friend WithEvents RBScrap As RadioButton
    Friend WithEvents RBTBRequest As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents BtnExport As Button
    Friend WithEvents BtnSearch As Button
    Friend WithEvents DTPSelectedDate As DateTimePicker
    Friend WithEvents CmbShift As ComboBox
    Friend WithEvents Panel1 As Panel
End Class
