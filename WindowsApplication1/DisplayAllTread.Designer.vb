﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayAllTread
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCode = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PKDEXTID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TREADCODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tread_Code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LEAF_TRUCKNO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LOCATION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TREADS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dual_Ext = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Display = New System.Windows.Forms.DataGridViewButtonColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.BackColor = System.Drawing.Color.LightPink
        Me.lblCode.Font = New System.Drawing.Font("Malgun Gothic", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.ForeColor = System.Drawing.Color.Maroon
        Me.lblCode.Location = New System.Drawing.Point(418, 18)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(226, 47)
        Me.lblCode.TabIndex = 123
        Me.lblCode.Text = "ALL TREADS"
        '
        'DataGridView1
        '
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PKDEXTID, Me.TREADCODE, Me.Tread_Code, Me.LEAF_TRUCKNO, Me.LOCATION, Me.TREADS, Me.Dual_Ext, Me.Display})
        Me.DataGridView1.Location = New System.Drawing.Point(113, 89)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(855, 572)
        Me.DataGridView1.TabIndex = 130
        '
        'PKDEXTID
        '
        Me.PKDEXTID.HeaderText = "PKDEXTID"
        Me.PKDEXTID.Name = "PKDEXTID"
        '
        'TREADCODE
        '
        Me.TREADCODE.HeaderText = "TREADCODE"
        Me.TREADCODE.Name = "TREADCODE"
        '
        'Tread_Code
        '
        Me.Tread_Code.HeaderText = "Colour Code"
        Me.Tread_Code.Name = "Tread_Code"
        '
        'LEAF_TRUCKNO
        '
        Me.LEAF_TRUCKNO.HeaderText = "LEAF_TRUCKNO"
        Me.LEAF_TRUCKNO.Name = "LEAF_TRUCKNO"
        '
        'LOCATION
        '
        Me.LOCATION.HeaderText = "LOCATION"
        Me.LOCATION.Name = "LOCATION"
        '
        'TREADS
        '
        Me.TREADS.HeaderText = "TREADS"
        Me.TREADS.Name = "TREADS"
        '
        'Dual_Ext
        '
        Me.Dual_Ext.HeaderText = "Dual_Ext"
        Me.Dual_Ext.Name = "Dual_Ext"
        '
        'Display
        '
        Me.Display.HeaderText = "Display"
        Me.Display.Name = "Display"
        Me.Display.Text = "Display"
        Me.Display.UseColumnTextForButtonValue = True
        '
        'DisplayAllTread
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1091, 684)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.lblCode)
        Me.Name = "DisplayAllTread"
        Me.Text = "DisplayAllTread"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PKDEXTID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TREADCODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tread_Code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LEAF_TRUCKNO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LOCATION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TREADS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dual_Ext As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Display As System.Windows.Forms.DataGridViewButtonColumn
End Class
