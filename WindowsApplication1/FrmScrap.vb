﻿Imports QRCoder
Imports System.IO
Imports System.Drawing.Printing
Public Class FrmScrap
    Dim objcon As New Connections
    Dim dt As New DataTable
    Public SelectedShift As String
    Public SelectedDate As DateTime
    Public SelectedprodDate As DateTime
    Public ddt As DataTable
    Private Sub FrmScrap_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtId.Enabled = True
        txtItemCode.Enabled = False
        txtColourCode.Enabled = False
        txtLTruckNo.Enabled = False
        txtLocation.Enabled = False
        txtQty.Enabled = False
        txtNo.Enabled = False
        btnScrap.Enabled = False
        BindCurrentDateandShift()
        LblOperaor.Text = GlobalVariables.Username
        Lblname.Text = GlobalVariables.Name
    End Sub
    Private Sub BindCurrentDateandShift()
        Dim ShiftA As DateTime, ShiftB As DateTime, ShiftC As DateTime, EndDayTime As DateTime, NextDayStartTime As DateTime
        Dim now As DateTime = DateTime.Now
        Dim curTime As DateTime = DateTime.Now
        ShiftA = Convert.ToDateTime("06:01:00 AM")
        ShiftB = Convert.ToDateTime("02:01:00 PM")
        ShiftC = Convert.ToDateTime("10:01:00 PM")
        EndDayTime = Convert.ToDateTime("11:59:55 PM")
        NextDayStartTime = Convert.ToDateTime("12:00:00 AM")
        If curTime >= ShiftA AndAlso curTime < ShiftB Then

            GlobalVariables.Shift = "A"
        ElseIf curTime >= ShiftB AndAlso curTime < ShiftC Then
            GlobalVariables.Shift = "B"
        ElseIf curTime >= ShiftC AndAlso curTime <= EndDayTime Then
            GlobalVariables.Shift = "C"
        ElseIf curTime >= NextDayStartTime AndAlso curTime < ShiftA Then
            GlobalVariables.Shift = "C"
        Else
            GlobalVariables.Shift = "G"
        End If

        If DateTime.Now > Convert.ToDateTime("12:00:00 AM") AndAlso DateTime.Now < Convert.ToDateTime("06:00:00 AM") Then
            Dim dt As DateTime = DateTime.Now
            GlobalVariables.CurrentDate = dt.AddDays(-1).ToString("yyyy/MM/dd")
            GlobalVariables.time = DateTime.Now.ToString("HH:mm:ss")
        Else
            GlobalVariables.CurrentDate = DateTime.Now.ToString("yyyy/MM/dd")
            GlobalVariables.time = DateTime.Now.ToString("HH:mm:ss")
        End If

    End Sub
    Private Sub btnScrap_Click(sender As Object, e As EventArgs) Handles btnScrap.Click
        btnScrap.Enabled = False

        Dim pkextid As Single = objcon.get_DextScrapID()
        GlobalVariables.DescId = pkextid

        dt = objcon.HoldDetRetrieval().Tables(0)
        If dt.Rows.Count > 0 Then
            GlobalVariables.Id1 = Convert.ToString(dt.Rows(0).ItemArray(0))
            GlobalVariables.extdate = Convert.ToDateTime(dt.Rows(0).ItemArray(1)).ToString("yyyy/MM/dd")
            GlobalVariables.exttime = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("HH:mm:ss tt")

            GlobalVariables.Press = Convert.ToDateTime(dt.Rows(0).ItemArray(2)).ToString("hh:mm tt")

            GlobalVariables.extshift = Convert.ToString(dt.Rows(0).ItemArray(3))
            GlobalVariables.SelectionTreadCode = Convert.ToString(dt.Rows(0).ItemArray(4))
            GlobalVariables.description = Convert.ToString(dt.Rows(0).ItemArray(5))
            GlobalVariables.oldLeaf = Convert.ToString(dt.Rows(0).ItemArray(6))
            GlobalVariables.SerialNo = Convert.ToString(dt.Rows(0).ItemArray(6))
            GlobalVariables.Qty = Convert.ToString(dt.Rows(0).ItemArray(7))
            GlobalVariables.Location = Convert.ToString(dt.Rows(0).ItemArray(8))

            GlobalVariables.curdate = Convert.ToDateTime(dt.Rows(0).ItemArray(9)).ToString("yyyy/MM/dd HH:mm:ss")
            GlobalVariables.No = Convert.ToString(dt.Rows(0).ItemArray(22))

            GlobalVariables.ItemCode = Convert.ToString(dt.Rows(0).ItemArray(19))
            GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))
            GlobalVariables.Username = Convert.ToString(dt.Rows(0).ItemArray(21))

            GlobalVariables.HId = Convert.ToString(dt.Rows(0).ItemArray(29))

        End If


        GlobalVariables.OperatorNo = LblOperaor.Text

        'GlobalVariables.Qty = txtQty.Text

        'GlobalVariables.T1 = GlobalVariables.Code
        'GlobalVariables.T2 = txtLocation.Text
        'GlobalVariables.T3 = txtQty.Text
        'GlobalVariables.T4 = GlobalVariables.No

        'Dim C1 As Single = objcon.Check_EntryCount()
        'If C1 <= 0 Then

        objcon.insertTreadScrapEntery()
        objcon.insertTreadScrapStatus()
        objcon.TrueUpdateLocation()
        'objcon.TrueUpdateLeaftruck()

        If txtColourCode.Text.StartsWith("SW") Then
            GlobalVariables.Defect_Type = "SW"
            objcon.TrueUpdateLeaftruckSW()
        Else
            GlobalVariables.Defect_Type = "other"
            objcon.TrueUpdateLeaftruck()
        End If
        objcon.DeleteTreadH()



        txtId.Text = ""
        txtItemCode.Text = ""
        txtColourCode.Text = ""
        txtLTruckNo.Text = ""
        txtLocation.Text = ""
        txtQty.Text = ""
        txtNo.Text = ""


        '  End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        btnScrap.Enabled = False
        txtId.Text = ""
        txtItemCode.Text = ""
        txtColourCode.Text = ""
        txtLTruckNo.Text = ""
        txtLocation.Text = ""
        txtQty.Text = ""
        txtNo.Text = ""
      
    End Sub

    Private Sub btnEnter_Click(sender As Object, e As EventArgs) Handles btnEnter.Click
        If txtId.Text <> "" Then
            btnScrap.Enabled = True
            'txtQty.Enabled = True
            'CboLocation.Enabled = True
            'CboLeafTruck.Enabled = True
            GlobalVariables.id = txtId.Text
            dt = objcon.HoldDetRetrieval().Tables(0)
            If dt.Rows.Count > 0 Then
                txtItemCode.Text = Convert.ToString(dt.Rows(0).ItemArray(4))
                txtColourCode.Text = Convert.ToString(dt.Rows(0).ItemArray(19))
                txtLTruckNo.Text = Convert.ToString(dt.Rows(0).ItemArray(6))
                txtLocation.Text = Convert.ToString(dt.Rows(0).ItemArray(8))
                txtQty.Text = Convert.ToString(dt.Rows(0).ItemArray(7))
                txtNo.Text = Convert.ToString(dt.Rows(0).ItemArray(22))


              

                GlobalVariables.SelectionTreadCode = txtItemCode.Text
                GlobalVariables.DualSelection = Convert.ToString(dt.Rows(0).ItemArray(20))

                'ddt = objcon.GetCompound().Tables(0)
                'txtLocation.Text = Convert.ToString(ddt.Rows(0).ItemArray(8)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(9)) + "/" + Convert.ToString(ddt.Rows(0).ItemArray(10))
                'GlobalVariables.Tyre = txtLocation.Text

             

            Else
                MessageBox.Show("No Data Found")
                txtId.Text = ""
                btnScrap.Enabled = False
                txtItemCode.Text = ""
                txtColourCode.Text = ""
                txtLTruckNo.Text = ""
                txtLocation.Text = ""
                txtQty.Text = ""
                txtNo.Text = ""
                txtLTruckNo.Text = ""
            End If
        Else
            MessageBox.Show("Enter the Id")
        End If
    End Sub
End Class