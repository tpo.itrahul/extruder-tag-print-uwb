﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Stock_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.R1 = New System.Windows.Forms.RadioButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.R2 = New System.Windows.Forms.RadioButton()
        Me.R3 = New System.Windows.Forms.RadioButton()
        Me.T1 = New System.Windows.Forms.TextBox()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.PeachPuff
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Purple
        Me.Label1.Location = New System.Drawing.Point(406, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(431, 36)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "CURRENT STOCK REPORT"
        '
        'R1
        '
        Me.R1.AutoSize = True
        Me.R1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.R1.ForeColor = System.Drawing.Color.Navy
        Me.R1.Location = New System.Drawing.Point(331, 84)
        Me.R1.Name = "R1"
        Me.R1.Size = New System.Drawing.Size(96, 24)
        Me.R1.TabIndex = 61
        Me.R1.TabStop = True
        Me.R1.Text = "Location"
        Me.R1.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Thistle
        Me.Label13.Font = New System.Drawing.Font("Malgun Gothic", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkMagenta
        Me.Label13.Location = New System.Drawing.Point(129, 77)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(178, 31)
        Me.Label13.TabIndex = 67
        Me.Label13.Text = "TYPE :"
        '
        'R2
        '
        Me.R2.AutoSize = True
        Me.R2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.R2.ForeColor = System.Drawing.Color.Navy
        Me.R2.Location = New System.Drawing.Point(492, 84)
        Me.R2.Name = "R2"
        Me.R2.Size = New System.Drawing.Size(59, 24)
        Me.R2.TabIndex = 68
        Me.R2.TabStop = True
        Me.R2.Text = "Age"
        Me.R2.UseVisualStyleBackColor = True
        '
        'R3
        '
        Me.R3.AutoSize = True
        Me.R3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.R3.ForeColor = System.Drawing.Color.Navy
        Me.R3.Location = New System.Drawing.Point(617, 83)
        Me.R3.Name = "R3"
        Me.R3.Size = New System.Drawing.Size(126, 24)
        Me.R3.TabIndex = 69
        Me.R3.TabStop = True
        Me.R3.Text = "Colour Code"
        Me.R3.UseVisualStyleBackColor = True
        '
        'T1
        '
        Me.T1.BackColor = System.Drawing.Color.PowderBlue
        Me.T1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.T1.ForeColor = System.Drawing.Color.DarkBlue
        Me.T1.Location = New System.Drawing.Point(617, 113)
        Me.T1.Name = "T1"
        Me.T1.Size = New System.Drawing.Size(126, 26)
        Me.T1.TabIndex = 70
        '
        'BtnSearch
        '
        Me.BtnSearch.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSearch.Location = New System.Drawing.Point(858, 96)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(131, 43)
        Me.BtnSearch.TabIndex = 71
        Me.BtnSearch.Text = "SEARCH"
        Me.BtnSearch.UseVisualStyleBackColor = False

        'BtnExport
        '
        Me.btnExport.BackColor = System.Drawing.Color.LavenderBlush
        Me.btnExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.Location = New System.Drawing.Point(1000, 96)
        Me.btnExport.Name = "BtnExport"
        Me.btnExport.Size = New System.Drawing.Size(131, 43)
        Me.btnExport.TabIndex = 71
        Me.btnExport.Text = "EXPORT"
        Me.btnExport.UseVisualStyleBackColor = False

        '
        'DataGridView1
        '
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 157)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1500, 600)
        Me.DataGridView1.TabIndex = 72
        Me.DataGridView1.Visible = False
        '
        'Stock_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Pink
        Me.ClientSize = New System.Drawing.Size(1600, 781)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.BtnSearch)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.T1)
        Me.Controls.Add(Me.R3)
        Me.Controls.Add(Me.R2)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.R1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Stock_Report"
        Me.Text = "Stock_Report"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents R1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents R2 As System.Windows.Forms.RadioButton
    Friend WithEvents R3 As System.Windows.Forms.RadioButton
    Friend WithEvents T1 As System.Windows.Forms.TextBox
    Friend WithEvents BtnSearch As System.Windows.Forms.Button
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
End Class
