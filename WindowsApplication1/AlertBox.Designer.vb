﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AlertBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnCntn = New System.Windows.Forms.Button()
        Me.CloseBTn = New System.Windows.Forms.Button()
        Me.AlertTextBox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'BtnCntn
        '
        Me.BtnCntn.Location = New System.Drawing.Point(73, 153)
        Me.BtnCntn.Name = "BtnCntn"
        Me.BtnCntn.Size = New System.Drawing.Size(124, 38)
        Me.BtnCntn.TabIndex = 0
        Me.BtnCntn.Text = "Continue"
        Me.BtnCntn.UseVisualStyleBackColor = True
        '
        'CloseBTn
        '
        Me.CloseBTn.Location = New System.Drawing.Point(265, 153)
        Me.CloseBTn.Name = "CloseBTn"
        Me.CloseBTn.Size = New System.Drawing.Size(124, 38)
        Me.CloseBTn.TabIndex = 1
        Me.CloseBTn.Text = "Close"
        Me.CloseBTn.UseVisualStyleBackColor = True
        '
        'AlertTextBox
        '
        Me.AlertTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.AlertTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AlertTextBox.Location = New System.Drawing.Point(12, 12)
        Me.AlertTextBox.Multiline = True
        Me.AlertTextBox.Name = "AlertTextBox"
        Me.AlertTextBox.Size = New System.Drawing.Size(428, 105)
        Me.AlertTextBox.TabIndex = 2
        Me.AlertTextBox.Text = "Wrong Leaf Truck Please Check"
        '
        'AlertBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(466, 233)
        Me.Controls.Add(Me.AlertTextBox)
        Me.Controls.Add(Me.CloseBTn)
        Me.Controls.Add(Me.BtnCntn)
        Me.Name = "AlertBox"
        Me.Text = "AlertBox"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnCntn As Button
    Friend WithEvents CloseBTn As Button
    Friend WithEvents AlertTextBox As TextBox
End Class
