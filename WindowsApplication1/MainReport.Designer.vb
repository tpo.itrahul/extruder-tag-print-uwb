﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCurrent = New System.Windows.Forms.Button()
        Me.BtnTakentoTB = New System.Windows.Forms.Button()
        Me.BtnProduction = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCurrent
        '
        Me.btnCurrent.BackColor = System.Drawing.Color.LightSeaGreen
        Me.btnCurrent.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCurrent.Location = New System.Drawing.Point(48, 170)
        Me.btnCurrent.Name = "btnCurrent"
        Me.btnCurrent.Size = New System.Drawing.Size(233, 59)
        Me.btnCurrent.TabIndex = 88
        Me.btnCurrent.Text = "Current Stock"
        Me.btnCurrent.UseVisualStyleBackColor = False
        '
        'BtnTakentoTB
        '
        Me.BtnTakentoTB.BackColor = System.Drawing.Color.LightSeaGreen
        Me.BtnTakentoTB.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnTakentoTB.Location = New System.Drawing.Point(308, 170)
        Me.BtnTakentoTB.Name = "BtnTakentoTB"
        Me.BtnTakentoTB.Size = New System.Drawing.Size(233, 59)
        Me.BtnTakentoTB.TabIndex = 89
        Me.BtnTakentoTB.Text = "Taken To TB"
        Me.BtnTakentoTB.UseVisualStyleBackColor = False
        '
        'BtnProduction
        '
        Me.BtnProduction.BackColor = System.Drawing.Color.LightSeaGreen
        Me.BtnProduction.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnProduction.Location = New System.Drawing.Point(566, 170)
        Me.BtnProduction.Name = "BtnProduction"
        Me.BtnProduction.Size = New System.Drawing.Size(233, 59)
        Me.BtnProduction.TabIndex = 90
        Me.BtnProduction.Text = "Production"
        Me.BtnProduction.UseVisualStyleBackColor = False
        '
        'MainReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PeachPuff
        Me.ClientSize = New System.Drawing.Size(852, 457)
        Me.Controls.Add(Me.BtnProduction)
        Me.Controls.Add(Me.BtnTakentoTB)
        Me.Controls.Add(Me.btnCurrent)
        Me.Name = "MainReport"
        Me.Text = "MainReport"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCurrent As System.Windows.Forms.Button
    Friend WithEvents BtnTakentoTB As System.Windows.Forms.Button
    Friend WithEvents BtnProduction As System.Windows.Forms.Button
End Class
