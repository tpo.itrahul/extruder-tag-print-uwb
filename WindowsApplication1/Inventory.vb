﻿Imports Microsoft.Office.Interop

Public Class Inventory
    Dim DTSpec As DataTable
    Dim objcon As New Connections
    Dim DualExactDate As Date
    Dim DualExactShift As String
    Public ds As New DataSet()
    Dim DtStock As DataTable
    Dim rwcnt As Integer = 0
    Public dt As New DataTable()
    Dim excelLocation As String
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Private Sub Inventory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        GetInventory()
    End Sub

    Public Sub GetInventory()
        DateTimelbl.Text = DateTime.Now.ToString("dd-MM-yyyy h:mm tt")

        dt.Rows.Clear()
        dt.Columns.Clear()
        rwcnt = 0
        DataGridinventory.Visible = True

        dt.Columns.Add("GT CODE", GetType(String))
        dt.Columns.Add("SAP CODE", GetType(String))
        dt.Columns.Add("AGED TREADS", GetType(String))
        dt.Columns.Add("UNDERAGED TREAD QNTY", GetType(String))
        dt.Columns.Add("OVERAGED TREAD QNTY", GetType(String))
        dt.Columns.Add("TOTAL TREAD QNTY", GetType(String))

        ds = objcon.GETInventoryData()
        If ds.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                Dim dr As DataRow = dt.NewRow()
                dt.Rows.Add(dr)
                'Dim Value_row
                'If IsDBNull(ds.Tables(0).Rows(i).ItemArray(0) Or ds.Tables(0).Rows(i).ItemArray(1)) Then
                '    If IsDBNull(ds.Tables(0).Rows(i).ItemArray(0)) Then

                '    Else

                '    End If
                'ElseIf IsDBNull(dt.Rows(rwcnt)(0)) Then
                '    Value_row = Convert.ToInt32(dt.Rows(rwcnt)(1))
                'Else
                '    dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                'End If

                dt.Rows(rwcnt)(0) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(0))
                dt.Rows(rwcnt)(1) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(1))
                dt.Rows(rwcnt)(2) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(2))
                dt.Rows(rwcnt)(3) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(3))
                dt.Rows(rwcnt)(4) = Convert.ToString(ds.Tables(0).Rows(i).ItemArray(4))
                'Dim Value_2 = 0
                'Dim Value_3 = 0
                'Dim Value_4 = 0

                'If IsDBNull(dt.Rows(rwcnt)(3)) Then

                '    Value_3 = 0
                'Else
                '    Value_3 = Convert.ToInt32(dt.Rows(rwcnt)(3))
                'End If
                'If IsDBNull(dt.Rows(rwcnt)(2)) Then

                '    Value_2 = 0
                'Else
                '    Value_2 = Convert.ToInt32(dt.Rows(rwcnt)(2))
                'End If

                'If IsDBNull(dt.Rows(rwcnt)(4)) Then

                '    Value_4 = 0
                'Else
                '    Value_4 = Convert.ToInt32(dt.Rows(rwcnt)(4))
                'End If




                ' dt.Rows(rwcnt)(5) = Value_2+Value_3 + Value_4
                dt.Rows(rwcnt)(5) = Convert.ToInt32(dt.Rows(rwcnt)(2)) + Convert.ToInt32(dt.Rows(rwcnt)(3)) + Convert.ToInt32(dt.Rows(rwcnt)(4))

                '  dt.Rows(rwcnt)(5) = If IsNullOrEmpty(dt.Rows(rwcnt)(3).ToString()))

                rwcnt += 1
            Next
            DataGridinventory.DataSource = dt

        End If
    End Sub
    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub BtnBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        Me.Close()
        FrmSelection.Show()
    End Sub

    Private Sub BtnRefresh_Click(sender As Object, e As EventArgs) Handles BtnRefresh.Click
        GetInventory()
    End Sub

    Private Sub BtnExport_Click(sender As Object, e As EventArgs) Handles BtnExport.Click
        If DataGridinventory.Rows.Count <= 2 Then
            MsgBox("Nothing to Export")
            Exit Sub

        End If
        Dim saveDlg As New System.Windows.Forms.SaveFileDialog()

        saveDlg.InitialDirectory = "C:\"
        saveDlg.Filter = "Excel files (*.xlsx)|*.xlsx"
        saveDlg.FilterIndex = 0
        saveDlg.RestoreDirectory = True
        saveDlg.Title = "Export Excel File To"

        If saveDlg.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            workbook = APP.Workbooks.Open("c:\Temp\File.xlsx")
            worksheet = workbook.Worksheets("Sheet1")

            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).Merge()
            worksheet.Range(worksheet.Cells(1, 1), worksheet.Cells(1, 10)).VerticalAlignment = Excel.Constants.xlCenter
            worksheet.Cells(1, 1).Value = "  Inventory Report   "
            Dim columnsCount As Integer = DataGridinventory.Columns.Count
            For Each column In DataGridinventory.Columns
                worksheet.Cells(2, column.Index + 1).Value = column.Name
                worksheet.Columns.AutoFit()
            Next
            'Export Header Name End


            'Export Each Row Start
            For i As Integer = 0 To DataGridinventory.Rows.Count - 2
                Dim columnIndex As Integer = 0
                Do Until columnIndex = columnsCount
                    worksheet.Cells(i + 3, columnIndex + 1).Value = DataGridinventory.Item(columnIndex, i).Value.ToString
                    columnIndex += 1
                Loop
            Next
            'Export Each Row End

            excelLocation = saveDlg.FileName
            workbook.SaveCopyAs(excelLocation)
            workbook.Saved = True
            workbook.Close(True, 0, 0)
            APP.Quit()

            MessageBox.Show("Exporting is completed..")
        End If

    End Sub
End Class