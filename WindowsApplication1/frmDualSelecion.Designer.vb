﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDualSelecion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDual1 = New System.Windows.Forms.Button()
        Me.btnDual2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BtnPreviousShift = New System.Windows.Forms.Button()
        Me.btnCurrentShift = New System.Windows.Forms.Button()
        Me.lblShift = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnDual1
        '
        Me.btnDual1.BackColor = System.Drawing.Color.DarkMagenta
        Me.btnDual1.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDual1.ForeColor = System.Drawing.SystemColors.Control
        Me.btnDual1.Location = New System.Drawing.Point(76, 58)
        Me.btnDual1.Name = "btnDual1"
        Me.btnDual1.Size = New System.Drawing.Size(215, 60)
        Me.btnDual1.TabIndex = 0
        Me.btnDual1.Text = "DUAL 1"
        Me.btnDual1.UseVisualStyleBackColor = False
        '
        'btnDual2
        '
        Me.btnDual2.BackColor = System.Drawing.Color.DarkMagenta
        Me.btnDual2.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDual2.ForeColor = System.Drawing.SystemColors.Control
        Me.btnDual2.Location = New System.Drawing.Point(314, 58)
        Me.btnDual2.Name = "btnDual2"
        Me.btnDual2.Size = New System.Drawing.Size(215, 60)
        Me.btnDual2.TabIndex = 1
        Me.btnDual2.Text = "DUAL 2"
        Me.btnDual2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSalmon
        Me.Panel1.Controls.Add(Me.BtnPreviousShift)
        Me.Panel1.Controls.Add(Me.btnCurrentShift)
        Me.Panel1.Location = New System.Drawing.Point(174, 210)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(262, 167)
        Me.Panel1.TabIndex = 2
        '
        'BtnPreviousShift
        '
        Me.BtnPreviousShift.BackColor = System.Drawing.Color.Indigo
        Me.BtnPreviousShift.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPreviousShift.ForeColor = System.Drawing.SystemColors.Control
        Me.BtnPreviousShift.Location = New System.Drawing.Point(24, 89)
        Me.BtnPreviousShift.Name = "BtnPreviousShift"
        Me.BtnPreviousShift.Size = New System.Drawing.Size(215, 60)
        Me.BtnPreviousShift.TabIndex = 5
        Me.BtnPreviousShift.Text = "Previous Shift"
        Me.BtnPreviousShift.UseVisualStyleBackColor = False
        '
        'btnCurrentShift
        '
        Me.btnCurrentShift.BackColor = System.Drawing.Color.Indigo
        Me.btnCurrentShift.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCurrentShift.ForeColor = System.Drawing.SystemColors.Control
        Me.btnCurrentShift.Location = New System.Drawing.Point(24, 23)
        Me.btnCurrentShift.Name = "btnCurrentShift"
        Me.btnCurrentShift.Size = New System.Drawing.Size(215, 60)
        Me.btnCurrentShift.TabIndex = 4
        Me.btnCurrentShift.Text = "Current Shift"
        Me.btnCurrentShift.UseVisualStyleBackColor = False
        '
        'lblShift
        '
        Me.lblShift.AutoSize = True
        Me.lblShift.Location = New System.Drawing.Point(476, 160)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(0, 13)
        Me.lblShift.TabIndex = 5
        '
        'frmDualSelecion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(639, 461)
        Me.Controls.Add(Me.lblShift)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnDual2)
        Me.Controls.Add(Me.btnDual1)
        Me.Name = "frmDualSelecion"
        Me.Text = "frmDualSelecion"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnDual1 As System.Windows.Forms.Button
    Friend WithEvents btnDual2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BtnPreviousShift As System.Windows.Forms.Button
    Friend WithEvents btnCurrentShift As System.Windows.Forms.Button
    Friend WithEvents lblShift As System.Windows.Forms.Label
End Class
